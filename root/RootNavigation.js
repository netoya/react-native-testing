import {View, Text} from 'react-native';
import React from 'react';
import ExampleNavigationStack from '../modules/examples/handlers/ExampleNavigation';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeNavigationStack from '../modules/home/handlers/HomeNavigation';
import NavigationNavigationStack from '../modules/navigation/handlers/NavigationNavigation';

const Stack = createNativeStackNavigator();

const RootNavigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        {HomeNavigationStack(Stack)}
        {ExampleNavigationStack(Stack)}
        {NavigationNavigationStack(Stack)}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigation;
