import {View, Text, TouchableOpacity, Switch} from 'react-native';
import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';

export const NavigationHomeScreen = props => {
  const navigation = useNavigation();

  const [switchValue, setSwitchValue] = useState(true);
  return (
    <View>
      <Text>NavigationScreen</Text>
      <TouchableOpacity
        testID="link-home"
        onPress={() => {
          // use props.navigation or navigation hook
          props.navigation.navigate('Home');
        }}>
        <Text>Name</Text>
      </TouchableOpacity>

      <TouchableOpacity
        testID="link-with-params"
        onPress={() => {
          navigation.navigate('Home', {
            paramExists: 'check property exists',
            paramValue: 20, // check property value equals
          });
        }}>
        <Text>Name with params</Text>
      </TouchableOpacity>
      <TouchableOpacity
        testID="link-go-back"
        onPress={() => {
          navigation.goBack();
        }}>
        <Text>Go back</Text>
      </TouchableOpacity>
      <View
        style={{
          width: '100%',
          height: 1,
          backgroundColor: 'black',
        }}></View>
      <Text>Swith on: ExampleHomeScreen</Text>
      <Text>Swith off: HomeScreen</Text>
      <Switch
        testID="switch-condition"
        value={switchValue}
        onValueChange={value => {
          setSwitchValue(value);
        }}></Switch>
      <Text>Current {switchValue ? 'Example' : 'Home'}</Text>

      <TouchableOpacity
        testID="link-with-condition"
        onPress={() => {
          let name = switchValue ? 'ExampleHome' : 'Home';
          navigation.navigate(name, {
            switchValue,
          });
        }}>
        <Text>Name with condicion</Text>
      </TouchableOpacity>
    </View>
  );
};
