import * as React from 'react';

import {render, screen, fireEvent} from '@testing-library/react-native';

// Navigation Mock
// Test init
import {describeNavigation} from '../../../../test_utils/describeNavigation';
import {NavigationHomeScreen} from '../NavigationHomeScreen';

describeNavigation('NavigationHomeScreen', props => {
  test('navigate without params', async () => {
    let {navigation} = props;
    render(<NavigationHomeScreen {...props} />);

    let expectedName = 'Home';
    fireEvent.press(screen.getByTestId('link-home'));
    expect(navigation.navigate).toHaveBeenCalledWith(expectedName);
  });

  test('navigate with params', async () => {
    let {navigation} = props;
    render(<NavigationHomeScreen {...props} />);

    let expectedName = 'Home';
    fireEvent.press(screen.getByTestId('link-with-params'));

    let callParams = navigation.navigate.mock.lastCall;
    expect(callParams[1]);
    expect(callParams[1]).toHaveProperty('paramExists');
    expect(callParams[1]).toHaveProperty('paramValue', 20);
  });

  test('navigate go back', async () => {
    let {navigation} = props;
    render(<NavigationHomeScreen {...props} />);

    fireEvent.press(screen.getByTestId('link-go-back'));
    expect(navigation.goBack).toHaveBeenCalled();
  });

  test('navigate with condition', async () => {
    let {navigation} = props;

    render(<NavigationHomeScreen {...props} />);
    let callParams = '';

    // Swith on
    fireEvent(screen.getByTestId('switch-condition'), 'onValueChange', true);
    fireEvent.press(screen.getByTestId('link-with-condition'));
    callParams = navigation.navigate.mock.lastCall;
    expect(callParams[0]).toEqual('ExampleHome');

    // Switch off
    fireEvent(screen.getByTestId('switch-condition'), 'onValueChange', false);
    fireEvent.press(screen.getByTestId('link-with-condition'));
    callParams = navigation.navigate.mock.lastCall;
    expect(callParams[0]).toEqual('Home');
  });
});
