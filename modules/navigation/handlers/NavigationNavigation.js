import React from 'react';
import {NavigationHomeScreen} from '../screens/NavigationHomeScreen';

const NavigationNavigationStack = Stack => {
  return (
    <Stack.Group>
      <Stack.Screen name="NavigationHome" component={NavigationHomeScreen} />
    </Stack.Group>
  );
};

export default NavigationNavigationStack;
