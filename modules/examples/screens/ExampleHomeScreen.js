import {Button, Text, TextInput, View} from 'react-native';

import React from 'react';

const ExampleHomeScreen = () => {
  const [name, setUser] = React.useState('');
  const [show, setShow] = React.useState(false);
  return (
    <View>
      <TextInput value={name} onChangeText={setUser} testID="input" />
      <Button
        title="Show username"
        onPress={() => {
          // let's pretend this is making a server request, so it's async
          // (you'd want to mock this imaginary request in your unit tests)...
          setTimeout(() => {
            setShow(true);
          }, Math.floor(Math.random() * 200));
        }}
      />
      {show && <Text testID="printed-username">{name}</Text>}
    </View>
  );
};

export default ExampleHomeScreen;
