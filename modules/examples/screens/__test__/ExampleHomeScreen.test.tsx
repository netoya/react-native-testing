import * as React from 'react';

import {render, screen, fireEvent} from '@testing-library/react-native';
import ExampleHomeScreen from '../ExampleHomeScreen';

test('examples of some things', async () => {
  const expectedUsername = 'Ada Lovelace';

  render(<ExampleHomeScreen />);

  fireEvent.changeText(screen.getByTestId('input'), expectedUsername);
  fireEvent.press(screen.getByText('Show username'));

  // Using `findBy` query to wait for asynchronous operation to finish
  const usernameOutput = await screen.findByTestId('printed-username');

  // Using `toHaveTextContent` matcher from `@testing-library/jest-native` package.
  expect(usernameOutput).toHaveTextContent(expectedUsername);

  expect(screen.toJSON()).toMatchSnapshot();
});
