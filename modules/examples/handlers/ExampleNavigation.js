import React from 'react';
import ExampleHomeScreen from '../screens/ExampleHomeScreen';

const ExampleNavigationStack = Stack => {
  return (
    <Stack.Group>
      <Stack.Screen name="ExampleHome" component={ExampleHomeScreen} />
    </Stack.Group>
  );
};

export default ExampleNavigationStack;
