import React from 'react';
import {HomeScreen} from '../screens/HomeScreen';

const HomeNavigationStack = Stack => {
  return (
    <Stack.Group>
      <Stack.Screen name="Home" component={HomeScreen} />
    </Stack.Group>
  );
};

export default HomeNavigationStack;
