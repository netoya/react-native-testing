import {View, Text, Button, TouchableOpacity} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';

export const HomeScreen = () => {
  const navigation = useNavigation();

  return (
    <View>
      <Text>HomeScreen</Text>
      <View>
        <TouchableOpacity
          testID="link-example"
          onPress={() => {
            navigation.navigate('ExampleHome');
          }}>
          <Text>Example test</Text>
        </TouchableOpacity>
        <TouchableOpacity
          testID="link-navigation"
          onPress={() => {
            navigation.navigate('NavigationHome');
          }}>
          <Text>Navigation test</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
