import * as React from 'react';

import {render, screen, fireEvent} from '@testing-library/react-native';

// Navigation Mock
// Test init
import {describeNavigation} from '../../../../test_utils/describeNavigation';
import {HomeScreen} from '../HomeScreen';

describeNavigation('HomeScreen', props => {
  test('Home links', async () => {
    let {navigation} = props;
    render(<HomeScreen {...props} />);

    let expectedName;

    expectedName = 'ExampleHome';
    fireEvent.press(screen.getByTestId('link-example'));
    expect(navigation.navigate).toHaveBeenCalledWith(expectedName);

    expectedName = 'NavigationHome';
    fireEvent.press(screen.getByTestId('link-navigation'));
    expect(navigation.navigate).toHaveBeenCalledWith(expectedName);
  });
});
