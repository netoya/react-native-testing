import {NavigationProp} from '@react-navigation/native';

type NavigationPropAlias = NavigationProp<{}>;

let props = {};

let navigation: Partial<NavigationPropAlias> = {};
jest.doMock('@react-navigation/native', () => {
  const actualNav = jest.requireActual('@react-navigation/native');
  return {
    ...actualNav,
    useNavigation: () => {
      return navigation;
    },
  };
});

const beforeEachNavigation = (props: any) => () => {
  navigation = Object.assign(navigation, {
    dispatch: jest.fn(),
    navigate: jest.fn(),
    goBack: jest.fn(),
  });

  props = Object.assign(props, {navigation: navigation});
};

export const describeNavigation = (name: any, func: (props: any) => any) => {
  describe(name, () => {
    beforeEach(beforeEachNavigation(props));
    func(props);
  });
};
